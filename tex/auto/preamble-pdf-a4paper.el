(TeX-add-style-hook
 "preamble-pdf-a4paper"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("ltjsbook" "twoside" "14Q" "a4paper" "openany")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("fontspec" "no-math") ("hyperref" "unicode=true" "hyperfootnotes=false" "pageanchor") ("footmisc" "bottom" "perpage" "stable")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "sauces-index-def"
    "ltjsbook"
    "ltjsbook10"
    "amsmath"
    "amssymb"
    "fontspec"
    "geometry"
    "luaotfload"
    "graphicx"
    "setspace"
    "refcount"
    "hyperref"
    "unicode-math"
    "luatexja"
    "luatexja-fontspec"
    "luatexja-ruby"
    "layout"
    "multicol"
    "footmisc"
    "index"
    "etoolbox"
    "umoline"
    "xfrac")
   (TeX-add-symbols
    '("frsub" 1)
    '("frsecb" 1)
    '("frsec" 1)
    '("frchap" 1)
    "medlarge"
    "medsmall"
    "twelveq"
    "thirteenq"
    "fourteenq"
    "fifteenq"
    "maeaki"
    "atoaki"
    "footnote"
    "tightlist"
    "cite"
    "item"
    "myhyperlink"
    "ul"
    "frac"
    "chaptermark"
    "sectionmark")
   (LaTeX-add-environments
    "equation"
    "frchapenv"
    "frsecbenv"
    "frsecenv"
    "frsubenv"
    "Main"
    "recette")
   (LaTeX-add-pagestyles
    "headings"))
 :latex)

