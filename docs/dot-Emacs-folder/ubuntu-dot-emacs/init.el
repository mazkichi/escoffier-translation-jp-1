(setq package-archives
      '(("gnu" . "http://elpa.gnu.org/packages/")
        ("melpa" . "http://melpa.org/packages/")))
;;        ("org" . "http://orgmode.org/elpa/")))
(package-initialize)
;(init-loader-load)
;(global-set-key [?\C\ ] 'set-mark-command)


(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default-input-method "french-azerty")
 '(package-selected-packages
   (quote
    (markdown-mode fontawesome magit auctex auto-complete line-reminder visual-regexp-steroids visual-regexp flymake-lua lua-mode ac-haskell-process flycheck-haskell flymake-haskell-multi ghc ghc-imported-from haskell-emacs haskell-emacs-base haskell-emacs-text haskell-mode haskell-snippets haskell-tab-indent shell-pop auto-yasnippet el-autoyas helm-c-yasnippet yasnippet yasnippet-classic-snippets yasnippet-snippets ac-helm ac-ispell ace-flyspell ac-skk atom-one-dark-theme flyspell-correct-helm flyspell-correct-ivy find-file-in-project tablist auto-save-buffers-enhanced ivy popwin undo-tree undohist tabbar solarized-theme sequential-command pandoc-mode package-utils neotree material-theme init-loader hlinum helm exec-path-from-shell ddskk auctex-latexmk ample-zen-theme all-the-icons)))
 '(shell-pop-default-directory "/Users/manabu/Bitbucket/escoffier-translation-jp/")
 '(shell-pop-full-span t)
 '(shell-pop-shell-type
   (quote
    ("ansi-term" "*ansi-term*"
     (lambda nil
       (ansi-term shell-pop-term-shell)))))
 '(shell-pop-term-shell "/bin/bash")
 '(shell-pop-universal-key "C-t")
 '(shell-pop-window-position "bottom")
 '(shell-pop-window-size 30))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(font-latex-sectioning-5-face ((t (:inherit variable-pitch :foreground "grey22"))))
 '(linum-highlight-face ((t (:inherit default :background "#586e75" :foreground "black"))))
 '(markdown-header-face ((t (:weight bold :foreground "#9e239c"))))
 '(markdown-header-face-1 ((t (:height 1.6))))
 '(markdown-header-face-2 ((t (:height 1.4))))
 '(markdown-header-face-3 ((t (:height 1.2))))
 '(markdown-header-face-4 ((t (:height 1.0))))
 '(markdown-header-face-5 ((t (:height 1.0))))
 '(markdown-header-face-6 ((t (:height 1.0))))
 '(neo-banner-face ((t (:foreground "#93a1a1" :weight normal :height 0.9))))
 '(neo-dir-link-face ((t (:foreground "#2aa198"))))
 '(neo-file-link-face ((t (:foreground "#93a1a1"))))
 '(neo-header-face ((t (:foreground "#93a1a1"))))
 '(neo-root-dir-face ((t (:foreground "#cb4b16"))))
 '(neo-vc-default-face ((t (:foreground "#93a1a1")))))


(setq skk-use-color-cursor t)
 (setq skk-cursor-default-color "#dddddd")
 (setq skk-cursor-hiragana-color "#d33682")
 (setq skk-cursor-katakana-color "#859900")
 (setq skk-cursor-jisx0201-color "#6c71c4")
 (setq skk-cursor-jisx0208-latin-color "#b58990")
 (setq skk-cursor-latin-color "#bbbbbb")
 (setq skk-cursor-abbrev-color "#268bd2")
;; (set-face-foreground 'skk-emacs-hiragana-face skk-cursor-hiragana-color)
;; (set-face-foreground 'skk-emacs-katakana-face skk-cursor-katakana-color)
;; (set-face-foreground 'skk-emacs-jisx0201-face skk-cursor-jisx0201-color)
;; C-hデリート
;;(global-set-key "\C-h" 'delete-backward-char)
(define-key key-translation-map [?\C-h] [?\C-?])
