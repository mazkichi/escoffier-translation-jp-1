(yas-global-mode 1)
;;; スニペット名をidoで選択する
;(setq yas-prompt-functions '(yas-ido-prompt))
(require 'yasnippet)
(require 'helm-c-yasnippet)
(setq helm-yas-space-match-any-greedy t)
(global-set-key (kbd "C-c y") 'helm-yas-complete)
(push '("emacs.+/snippets/" . snippet-mode) auto-mode-alist)
(yas-global-mode 1)
